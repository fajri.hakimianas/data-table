import React from "react";
import BootstrapTable from "react-bootstrap-table-next";

const products = [];
const columns = [
  {
    dataField: "id",
    text: "Product ID",
  },
  {
    dataField: "name",
    text: "Product Name",
  },
  {
    dataField: "price",
    text: "Product Price",
  },
];

const Homepage = () => {
  return (
    <div>
      <BootstrapTable keyField="id" data={products} columns={columns} />
    </div>
  );
};

export default Homepage;
